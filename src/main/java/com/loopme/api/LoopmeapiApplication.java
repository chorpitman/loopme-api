package com.loopme.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoopmeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoopmeapiApplication.class, args);
	}
}
